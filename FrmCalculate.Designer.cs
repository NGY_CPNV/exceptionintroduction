﻿namespace ExceptionIntroduction
{
    partial class FrmCalculate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBOp1 = new System.Windows.Forms.TextBox();
            this.txtBOp2 = new System.Windows.Forms.TextBox();
            this.txtBResult = new System.Windows.Forms.TextBox();
            this.cmdCalculate = new System.Windows.Forms.Button();
            this.lblEqual = new System.Windows.Forms.Label();
            this.lstBOperator = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // txtBOp1
            // 
            this.txtBOp1.Location = new System.Drawing.Point(22, 12);
            this.txtBOp1.Name = "txtBOp1";
            this.txtBOp1.Size = new System.Drawing.Size(42, 20);
            this.txtBOp1.TabIndex = 0;
            this.txtBOp1.TextChanged += new System.EventHandler(this.txtBOp1_TextChanged);
            // 
            // txtBOp2
            // 
            this.txtBOp2.Location = new System.Drawing.Point(117, 12);
            this.txtBOp2.Name = "txtBOp2";
            this.txtBOp2.Size = new System.Drawing.Size(42, 20);
            this.txtBOp2.TabIndex = 1;
            this.txtBOp2.TextChanged += new System.EventHandler(this.txtBOp2_TextChanged);
            // 
            // txtBResult
            // 
            this.txtBResult.Enabled = false;
            this.txtBResult.Location = new System.Drawing.Point(200, 12);
            this.txtBResult.Name = "txtBResult";
            this.txtBResult.Size = new System.Drawing.Size(42, 20);
            this.txtBResult.TabIndex = 2;
            // 
            // cmdCalculate
            // 
            this.cmdCalculate.Location = new System.Drawing.Point(98, 43);
            this.cmdCalculate.Name = "cmdCalculate";
            this.cmdCalculate.Size = new System.Drawing.Size(75, 23);
            this.cmdCalculate.TabIndex = 3;
            this.cmdCalculate.Text = "Calculate";
            this.cmdCalculate.UseVisualStyleBackColor = true;
            this.cmdCalculate.Click += new System.EventHandler(this.cmdCalculate_Click);
            // 
            // lblEqual
            // 
            this.lblEqual.AutoSize = true;
            this.lblEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEqual.Location = new System.Drawing.Point(172, 9);
            this.lblEqual.Name = "lblEqual";
            this.lblEqual.Size = new System.Drawing.Size(24, 25);
            this.lblEqual.TabIndex = 5;
            this.lblEqual.Text = "=";
            // 
            // lstBOperator
            // 
            this.lstBOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBOperator.FormattingEnabled = true;
            this.lstBOperator.ItemHeight = 20;
            this.lstBOperator.Location = new System.Drawing.Point(71, 10);
            this.lstBOperator.Name = "lstBOperator";
            this.lstBOperator.Size = new System.Drawing.Size(40, 24);
            this.lstBOperator.TabIndex = 6;
            // 
            // FrmCalculate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 79);
            this.Controls.Add(this.lstBOperator);
            this.Controls.Add(this.lblEqual);
            this.Controls.Add(this.cmdCalculate);
            this.Controls.Add(this.txtBResult);
            this.Controls.Add(this.txtBOp2);
            this.Controls.Add(this.txtBOp1);
            this.Name = "FrmCalculate";
            this.Text = "Calculate";
            this.Load += new System.EventHandler(this.FrmCalculate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBOp1;
        private System.Windows.Forms.TextBox txtBOp2;
        private System.Windows.Forms.TextBox txtBResult;
        private System.Windows.Forms.Button cmdCalculate;
        private System.Windows.Forms.Label lblEqual;
        private System.Windows.Forms.ListBox lstBOperator;
    }
}

