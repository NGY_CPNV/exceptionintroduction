﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionIntroduction
{
    /// <summary>
    /// This class is designed
    /// to provide some arithmetic methods
    /// to project dealing with simple
    /// mathematic problems
    /// </summary>
    public class Maths
    {
        #region private attributes
        private int _op1 = 0;
        private int _op2 = 0;
        private char _operator;
        #endregion private attributes

        /// <summary>
        /// This constructor return a Maths object
        /// </summary>
        /// <param name="op1">Contains the first operand</param>
        /// <param name="op2">Contains the second operand</param>
        #region constructor
        public Maths(int op1, int op2, char oper)
        {
            _op1 = op1;
            _op2 = op2;
            _operator = oper;
        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// This method is designed
        /// to provide arithmetic operation
        /// </summary>
        /// <returns></returns>
        public float calculate()
        {
            float result = 0;
            switch (this._operator)
            {
                case '/':
                    result = divide();
                    break;
                case '*':
                    result = multiply();
                    break;
                case '+':
                    result = addition();
                    break;
                case '-':
                    result = substract();
                    break;
            }
            return result;
        }
        #endregion public methods

        #region private methods
        private float divide()
        {
            return this._op1 / this._op2;
        }

        private float multiply()
        {
            return this._op1 * this._op2;
        }

        private float addition()
        {
            return this._op1 + this._op2;
        }

        private float substract()
        {
            return this._op1 - this._op2;
        }
        #endregion private methods
    }
}
