﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExceptionIntroduction
{
    /// <summary>
    /// This test class is designed
    /// to test the Maths class
    /// </summary>
 	[TestClass]
    public class TestMaths
    {
        #region private attributes
        private int _operande1 = 0;
        private int _operande2 = 0;
        private char _operator;
        private float _resultExpected = 0;
        private float _resultActual = 0;
        #endregion private attributes

        #region init
        /// <summary>
        /// This test method is designed
        /// to be runned before each unit test
        /// in order to set all recurrent variables
        /// used in the set of test
        /// </summary>
        [TestInitialize]
        public void init()
        {
            _operande1 = 12;
            _operande2 = 4;
        }
        #endregion init

        #region unit tests
        /// <summary>
        /// This test method is designed
        /// to test the calculatee method when
        /// 2 intergers are filled in both txtBox
        /// We try to divide
        /// </summary>
        [TestMethod]
        public void Maths_Calculate_DivideTwoIntegers_Success()
        {
            //init
            this._operator = '/';
            this._resultActual = 0;
            this._resultExpected = 3;

            //given
            Maths maths = new Maths(this._operande1, this._operande2, this._operator);

            //when
            this._resultActual = maths.calculate();

            //then
            Assert.AreEqual(this._resultExpected, this._resultActual);
        }

        /// <summary>
        /// This test method is designed
        /// to test the calculatee method when
        /// 2 intergers are filled in both txtBox
        /// We try to multiply
        /// </summary>
        [TestMethod]
        public void Maths_Calculate_MultiplyTwoIntegers_Success()
        {
            //init
            this._operator = '*';
            this._resultActual = 0;
            this._resultExpected = 48;

            //given
            Maths maths = new Maths(this._operande1, this._operande2, this._operator);

            //when
            this._resultActual = maths.calculate();

            //then
            Assert.AreEqual(this._resultExpected, this._resultActual);
        }

        /// <summary>
        /// This test method is designed
        /// to test the calculatee method when
        /// 2 intergers are filled in both txtBox
        /// We try to addition
        /// </summary>
        [TestMethod]
        public void Maths_Calculate_AdditionTwoIntegers_Success()
        {
            //init
            this._operator = '+';
            this._resultActual = 0;
            this._resultExpected = 16;

            //given
            Maths maths = new Maths(this._operande1, this._operande2, this._operator);

            //when
            this._resultActual = maths.calculate();

            //then
            Assert.AreEqual(this._resultExpected, this._resultActual);
        }

        /// <summary>
        /// This test method is designed
        /// to test the calculatee method when
        /// 2 intergers are filled in both txtBox
        /// We try to substract
        /// </summary>
        [TestMethod]
        public void Maths_Calculate_SubstractTwoIntegers_Success()
        {
            //init
            this._operator = '-';
            this._resultActual = 0;
            this._resultExpected = 8;

            //given
            Maths maths = new Maths(this._operande1, this._operande2, this._operator);

            //when
            this._resultActual = maths.calculate();

            //then
            Assert.AreEqual(this._resultExpected, this._resultActual);
        }
        #endregion unit tests

        #region Cleanup
        /// <summary>
        /// This test method is designed
        /// to be runned after each unit test
        /// in order to clean all recurrent variables
        /// used in the set of test
        /// </summary>
        [TestCleanup]
        public void cleanUp()
        {
            this._operator = '\0';
            this._resultExpected = 0;
            this._resultActual = 0;
        }
        #endregion Cleanup
    }
}
