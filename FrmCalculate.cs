﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Log;

namespace ExceptionIntroduction
{
    /// <summary>
    /// This class is designed to set
    /// the GUI
    /// </summary>
    public partial class FrmCalculate : Form
    {
        #region constructors
        /// <summary>
        /// This method is a constructor
        /// responsible to initialize all graphic components needed
        /// to display the form FrmCalculate
        /// </summary>
        public FrmCalculate()
        {
            InitializeComponent();//method automatically written by C# to set the GUI
        }
        #endregion constructors

        #region GUI events
        private void FrmCalculate_Load(object sender, EventArgs e)
        {
            this.lstBOperator.Items.Add("+");//add value "+" in the list of operator
            this.lstBOperator.Items.Add("-");
            this.lstBOperator.Items.Add("*");
            this.lstBOperator.Items.Add("/");

            this.FormBorderStyle = FormBorderStyle.Fixed3D;//disable resizing for the user
            this.MaximizeBox = false;//disable maximize oprtion for the user
            this.txtBOp1.TabIndex = 1;//set the tab for the first operand
            this.lstBOperator.TabIndex = 3;//set the tab for the operator
            this.txtBOp2.TabIndex = 4;///set the tab for the second operand
            this.txtBResult.Enabled = false;//disable write rights on the result text box
            this.cmdCalculate.Enabled = false;//disable the button "calculate" (the operands 1 et 2 have to be filled before)
            this.lstBOperator.SelectedIndex = 0;//set the index 0 by default (to avoid the case "no operator selected")
        }

        private void txtBOp1_TextChanged(object sender, EventArgs e)
        {
            updateCmdCalculateStatus();//call the method designed to update the "calculate" button'status
        }

        private void txtBOp2_TextChanged(object sender, EventArgs e)
        {
            updateCmdCalculateStatus();//call the method designed to update the "calculate" button'status
        }

        private void cmdCalculate_Click(object sender, EventArgs e)
        {
            calculate();//call the method designed to calculate of the operation orders by the user
        }
        #endregion GUI events

        #region private methods
        private void updateCmdCalculateStatus()
        {
            if (this.txtBOp1.Text != "" && this.txtBOp2.Text != "")//we check if both operand are filled
            {
                this.txtBOp2.BackColor = Color.White;//set the backolor to withe (in case of previous error -> in red)
                this.cmdCalculate.Enabled = true;//if yes, we active the button calculate for the user
            }
            else
            {
                this.cmdCalculate.Enabled = false;//if no, we disable the button calculate for the user
            }
        }

        private void calculate()
        {
            int op1 = int.Parse(this.txtBOp1.Text);//get the value "op1" filled by the user
            int op2 = int.Parse(this.txtBOp2.Text);//need to convert the string collected in an integer (parse)
            char oper = lstBOperator.SelectedItem.ToString()[0];//get the current selected value operator
            float result;

            Maths maths = new Maths(op1, op2,oper);//init a object maths

            try
            {
                result = maths.calculate();//using the object "maths" to access to the divide method
                this.txtBResult.Text = result.ToString();//display the result in the GUI
            }
            catch (DivideByZeroException exMsg)
            {
                //business logic
                this.txtBOp2.BackColor = Color.Red;//set the backcolor in red to help user
                MessageBox.Show("Attention il n'est pas possible de diviser par zéro", "Division Impossible");//business error msg
                this.txtBOp2.Focus();//op2 will be focused
                this.cmdCalculate.Enabled = false;//btn calculate will be disable

                //technical logic
                Logger logger = new Logger();//init a logger object
                String logDirectoryPath = AppDomain.CurrentDomain.BaseDirectory;//set the current directory
                String logFileName = @"\calculateLogger.log";//set the log file name to use
                logger.writeInLogFile("Warning", logDirectoryPath, logFileName, exMsg.ToString());//write in log
            }
        }
        #endregion private methods
    }
}
